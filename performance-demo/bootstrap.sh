#!/usr/bin/env bash

apt-get -y -q update
apt-get -y -q upgrade
apt-get -y -q install software-properties-common htop
add-apt-repository ppa:webupd8team/java
apt-get -y -q update
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
apt-get -y -q install oracle-java8-installer

apt-get install -y -q git
apt-get install -y -q maven
git clone https://github.com/kuksenko/quantum.git
cd quantum
mvn clean package
java -jar target/benchmarks.jar quantum.demo7.PingPong0 -f 1 -wi 5 -i 5
