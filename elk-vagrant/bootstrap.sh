#!/usr/bin/env bash

#logstash
apt-get update
apt-get install  -y -q openjdk-7-jdk curl
curl -O https://download.elasticsearch.org/logstash/logstash/logstash-2.2.0.tar.gz
tar zxvf logstash-2.2.0.tar.gz

#elasticsearch
apt-get install -y -q unzip
curl -L -O https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-2.2.0.zip
unzip elasticsearch-2.2.0.zip

#kibana
curl -O https://download.elastic.co/kibana/kibana/kibana-4.4.2-linux-x64.tar.gz
tar zxvf kibana-4.4.2-linux-x64.tar.gz


